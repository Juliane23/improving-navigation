% THIS IS SIGPROC-SP.TEX - VERSION 3.1
% WORKS WITH V3.2SP OF ACM_PROC_ARTICLE-SP.CLS
% APRIL 2009
%
% It is an example file showing how to use the 'acm_proc_article-sp.cls' V3.2SP
% LaTeX2e document class file for Conference Proceedings submissions.
% ----------------------------------------------------------------------------------------------------------------
% This .tex file (and associated .cls V3.2SP) *DOES NOT* produce:
%       1) The Permission Statement
%       2) The Conference (location) Info information
%       3) The Copyright Line with ACM data
%       4) Page numbering
% ---------------------------------------------------------------------------------------------------------------
% It is an example which *does* use the .bib file (from which the .bbl file
% is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission,
% you need to 'insert'  your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.
%
% Questions regarding SIGS should be sent to
% Adrienne Griscti ---> griscti@acm.org
%
% Questions/suggestions regarding the guidelines, .tex and .cls files, etc. to
% Gerald Murray ---> murray@hq.acm.org
%
% For tracking purposes - this is V3.1SP - APRIL 2009

\documentclass{sig-alternate}
\usepackage{url}
\usepackage{graphicx}
\usepackage{color}
\newcommand{\mjw}[1]{\textcolor{blue}{MW: #1}}
\newcommand{\kim}[1]{\textcolor{red}{Kim: #1}}
\newcommand{\jf}[1]{\textcolor{magenta}{JF: #1}}


\begin{document}

\title{Do Academics Still Read Paper?}
%\titlenote{(Does NOT produce the permission block, copyright information nor page numbering). For use with ACM\_PROC\_ARTICLE-SP.CLS. Supported by ACM.}}
%\subtitle{[Extended Abstract]
%\titlenote{A full version of this paper is available as
%\textit{Author's Guide to Preparing ACM SIG Proceedings Using
%\LaTeX$2_\epsilon$\ and BibTeX} at
%\texttt{www.acm.org/eaddress.htm}}}
%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\numberofauthors{4} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Juliane Franze\\
       \affaddr{Caulfield School of IT}\\
       \affaddr{Monash University}\\
       \affaddr{Caulfield 3145, Australia}
% 2nd. author
\alignauthor
Kim Marriott\\
       \affaddr{Caulfield School of IT}\\
       \affaddr{Monash University}\\
       \affaddr{Caulfield 3145, Australia}
% 3rd. author
\alignauthor
Michael Wybrow\\
       \affaddr{Caulfield School of IT}\\
       \affaddr{Monash University}\\
       \affaddr{Caulfield 3145, Australia}
\and
       {\centering \email{\{Juliane.Franze, Kim.Marriott, Michael.Wybrow\}@monash.edu}}
}



% There's nothing stopping you putting the seventh, eighth, etc.
% author on the opening page (as the 'third row') but we ask,
% for aesthetic reasons that you place these 'additional authors'
% in the \additional authors block, viz.
%\additionalauthors{Additional authors: John Smith (The Th{\o}rv{\"a}ld Group,
%email: {\texttt{jsmith@affiliation.org}}) and Julius P.~Kumquat
%(The Kumquat Consortium, email: {\texttt{jpkumquat@consortium.net}}).}
\date{30 march 2014}
% Just remember to make sure that the TOTAL number of authors
% is the number that will appear on the first page PLUS the
% number that will appear in the \additionalauthors section.



\maketitle

%------- ABSTRACT --------------------
\begin{abstract}

% - Academic documents are a type of technical document.  Technical
%   documents are special because annotation is an important activity
%   and people frequently navigate the documents in non-linear ways.
%

Researchers must constantly read and annotate academic documents. While almost all documents are provided digitally, many are still printed and read on paper.  We surveyed 162 academics in order to better understand their reading habits and preferences. We were particularly interested in understanding why slightly more than half still preferred to work with printed papers rather than to read electronically. We present the findings of this survey and discuss the implications for the design of future digital reading applications that better cater for the needs of academic readers.


\end{abstract}

% A category with the (minimum) three required fields
%\category{H.4}{Information Systems Applications}{Miscellaneous}
%A category including the fourth, optional field follows...
%\category{D.2.8}{Software Engineering}{Metrics}[complexity measures, performance measures]

\category{H.1.2}{Models and Principles}{User/Machine Systems}[human factors]
\category{H.5.2}{Information Interfaces and Presentation}{User Interfaces}[evaluation/methodology]
% Maybe: 
% \category{I.7.0){Document and Text Processing}{General}

\terms{Experimentation, human Factors}

% NOT required for Proceedings
\keywords{Human behaviour, technical reading, usability, digital documents} 




%------- INTRODUCTION  --------------------
\section{Introduction}

Researchers spend significant amounts of time reading academic documents like conference papers, journal articles and textbooks. 
The majority of academic documents are provided digitally, most often as PDF files~\cite{marshall-2005, pettifer-2011}. 
Yet it seems that many researchers still prefer reading academic papers in print form. 

\begin{figure*}[t]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.38]{Data/Rplot1-FDU.pdf} &
\includegraphics[scale=0.38]{Data/Rplot1-MFDU.pdf}
\end{tabular}
\caption{Frequency of device use for reading academic papers (left), and number of times the device was preferred (i.e., most frequently used) by a participant (right). Key: Print (Pr); Desktop (DT); eReader (ER); Tablet (Tb); Smartphone (SP). Note that Smartphones were never preferred and respondents could choose more than one preferred device.
\label{fig:device-usage}}
\end{figure*}

\begin{figure*}[t]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.38]{Data/Rplot1-FDU-Age.pdf} &
\includegraphics[scale=0.38]{Data/Rplot1-MFDU-Age.pdf}  \\
\includegraphics[scale=0.38]{Data/Rplot1-FDU-Class.pdf} &
\includegraphics[scale=0.38]{Data/Rplot1-MFDU-Class.pdf} \\
\includegraphics[scale=0.38]{Data/Rplot1-FDU-Gender.pdf} &
\includegraphics[scale=0.38]{Data/Rplot1-MFDU-Gender.pdf} 
\end{tabular}
\caption{Frequency of different device use and number of times the device was preferred, i.e., usage was Most often, broken down by age, field of study and gender.\label{fig:device-usage-breakdown}}
\end{figure*}


To better understand the reading habits and reading demands of researchers, we conducted an online survey of 162 academics.
We asked the participants for their preferred reading media.
While reading digitally used to equate to reading at a desktop computer  the prevalence of tablets and smartphones means this is no longer the case and we were interested to investigate whether these new devices were being used for reading academic papers.

We found that paper and desktop computers are the most popular medium for reading academic papers, both being used about equally.  Smartphones are almost never used for reading, nor are E-readers.  The explanations given by participants from a variety of backgrounds were surprising for the strikingly similarity between participants.  As such, we feel this research presents the first complete, clear view of the reasons for the continued preference for paper use by academics and the low adoption for digital reading of academic documents on mobile devices.

%------- Discussion --------------------
\section{Survey}
\label{sec:results}

% \kim{Check the numbers for number participants giving gender, age and class data}
% Done -MJW

A total of 162 participant surveys were analysed. The gender distribution was slightly uneven: 58\% females to 42\% males, with 5 respondents not giving their gender. The sample consists of a diverse range of ages from under 25 to above 65.\footnote{One participant did not specify their age}. The distribution of ages is quite uneven, with 60\% of them being under 35, presumably reflecting the demographic for active researchers with the time to answer on-line surveys. The age bracket ``26 to 35'' contained the highest proportion with 41\% of the respondents. 

153 participants gave a field of study. We grouped these into 6 classes: 
\begin{itemize}
\item BIOMED (life sciences, health, psychology and psychiatry);
\item ICT (computer science, IT, telecommunications, information systems and knowledge management);
\item STEM (other sciences, engineering and mathematics);
\item NSTEM (everything else including the humanities, law and business and economics)
\end{itemize}


The first question on the survey asked respondents to check a box indicating how often they read academic papers using: \emph{Print}, \emph{EReader}, \emph{Desktop}, \emph{Tablet} and \emph{Smartphone}. The allowed responses were: \emph{Most often}, \emph{Often}, \emph{Occasionally} and \emph{Never}. 
		
From the responses we extracted two data sets: Frequency and Preferred. Frequency gave for each device a ranking of \emph{Never}, \emph{Occasionally} or \emph{Often} where \emph{Often} was true if the respondent had indicated either \emph{Most often} or \emph{Often} for that device. Preferred was a Boolean indicating if the participant response indicated that that medium was their most often used.
The results are shown in Figure~\ref{fig:device-usage}.  

We see that smartphones and eReaders are by far the most frequently used devices for reading academic papers, followed by tablets. Print and desktops are uncommon. We confirmed that this difference was significant at the $0.05$ confidence level.  

This device preference was supported by the data on most preferred device, i.e., Print and Desktop were used most often for reading academic papers, followed by tablets. Smartphones and eReaders are very uncommon. We used a $\chi^2$ test with equal probability null hypothesis ($\chi^2(4) = 159.6567$, $p < 2.2e-16$).  We used a two-way $\chi^2$ test and Fisher's exact test to examine if gender, age or field of study affected selection of Print or Desktop as the preferred device.We found that this significantly depended upon field of study ($\chi^2(6) = 14.0295$, $0.02931$; Fisher $p=0.02665$) but not on gender or age.

It is perhaps not surprising that smartphones are rarely used. Small screen sizes mean users need to scroll a lot, especially for PDF documents which are not reflowable. This causes increased effort and interruption in the reading flow for continuous reading. As a result, in-depth reading with a focus on comprehension is very difficult on such devices.  Similar issues occur for eReaders. This result reaffirms several previous studies including that of Hillesund~\cite{aaltonen-2011}.		
We then looked at whether gender, age or field of study affected device preference.
The results are shown in Figure~\ref{fig:device-usage-breakdown}.  
Because of the difficulty of performing a multi-way test on ranked data we used a Kruskal-Wallis $H$ test for significance differences for usage of each device. 
At the $0.05$ significance level we only found that  Tablet use depended upon Gender
($\chi^2(2) = 9.5357$, $p = 0.008499$).
We also tested for possible trends at the $0.10$ significance level, though of course these should be treated cautiously. We found:
EReader usage affected by gender ($\chi^2(2) = 5.3534$, $p = 0.06879$);
SPhone usage affected by gender ($\chi^2(2) = 5.724$, $p = 0.05715$);
DeskTop usage affected by field of study ($\chi^2(5) = 9.7402$, $p = 0.08294$);
Print usage affected by field of study ($\chi^2(5) = 9.2832$,  $p = 0.09829$).

It is surprising that tablet usage is dependent on gender and is much higher in females.  We notice that eReader usage is higher in females, though not statistically significant.  Our conjecture is that females may be more likely than males to own eReaders and prefer these for non-academic reading.  If this is the case, perhaps this preference carries over to academic reading.  More research is needed to determine the answer.


%------- Conclusion --------------------

\section{Conclusions}


Our results show an obvious usage gap between reading on mobile electronic devices and desktops, as well as between paper and mobile devices. Mobile devices have low usage for academic reading, especially eReaders and smartphones.  eReaders have fundamental issues, specifically slow refresh and black and white displays that make navigation and working with colour content problematic.  For tablets and smartphones the primary issue is lack of good annotation support in this environment.  Additionally, there are issues related to small screen sizes causing navigation and comfort issues with current fixed-layout documents.  Considering that smartphones and tablets are devices that people increasingly have with them and they often have internet connections and other relevant content on them, it seems important to concentrate future efforts on improving the experience of technical document reading on such devices.


%
% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\bibliographystyle{abbrv}
\bibliography{reading-habits}  
% You must have a proper ".bib" file
%  and remember to run:
% latex bibtex latex latex
% to resolve all references
%
% ACM needs 'a single self-contained file'!
%
%APPENDICES are optional
%\balancecolumns
% \appendix 
% Appendix A
% \section{Survey Questions}
% \label{sec:survey}
% Blah
% The rules about hierarchical headings discussed above for
% the body of the article are different in the appendices.
% In the \textbf{appendix} environment, the command
% \textbf{section} is used to
% indicate the start of each Appendix, with alphabetic order
% designation (i.e. the first is A, the second B, etc.) and
% a title (if you include one).  So, if you need
% hierarchical structure
% \textit{within} an Appendix, start with \textbf{subsection} as the
% highest level. Here is an outline of the body of this
% document in Appendix-appropriate form:
% \subsection{Introduction}
% \subsection{The Body of the Paper}
% \subsubsection{Type Changes and  Special Characters}
% \subsubsection{Math Equations}
% \paragraph{Inline (In-text) Equations}
% \paragraph{Display Equations}
% \subsubsection{Citations}
% \subsubsection{Tables}
% \subsubsection{Figures}
% \subsubsection{Theorem-like Constructs}
% \subsubsection*{A Caveat for the \TeX\ Expert}
% \subsection{Conclusions}
% \subsection{Acknowledgments}
% \subsection{Additional Authors}
% This section is inserted by \LaTeX; you do not insert it.
% You just add the names and information in the
% \texttt{{\char'134}additionalauthors} command at the start
% of the document.
% \subsection{References}
% Generated by bibtex from your ~.bib file.  Run latex,
% then bibtex, then latex twice (to resolve references)
% to create the ~.bbl file.  Insert that ~.bbl file into
% the .tex source file and comment out
% the command \texttt{{\char'134}thebibliography}.
% % This next section command marks the start of
% % Appendix B, and does not continue the present hierarchy
% \section{More Help for the Hardy}
% The acm\_proc\_article-sp document class file itself is chock-full of succinct
% and helpful comments.  If you consider yourself a moderately
% experienced to expert user of \LaTeX, you may find reading
% it useful but please remember not to change it.
% 
\balancecolumns
% That's all folks!

\end{document}
